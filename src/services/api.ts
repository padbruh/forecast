import axios from 'axios'
import { WeatherData } from '../types';

const API_KEY = '7e481a4ced9ea5606bc421fe33879c45';
const URL = `https://api.openweathermap.org/data/2.5/weather?&units=metric&exclude=minutely&appid=${API_KEY}`;

export const getCurrentWeatherData = async (lat: number, long: number) => {
  const params = `&lat=${lat}&lon=${long}&lang=pt_br`;
  const response = await axios.get<WeatherData>(`${URL}${params}`);
  return response.data;
}
import React, { useEffect, useState } from 'react';
import { View, ActivityIndicator, Alert, ImageBackground } from 'react-native';
import * as Location from 'expo-location';
import { getCurrentWeatherData } from './services/api';
import { LocationObject } from 'expo-location';
import { WeatherData } from './types';
import {
  ActivityContainer,
  Container,
  Footer,
  Header,
  HeaderLocation,
  Main,
  Temperature,
  TemperatureText,
  Text,
  ReloadButton,
} from './styles';
import { capitalize } from './utils';

import WorldMap from './assets/WorldMap/WorldMap.png';

export const Application = () => {
  const [location, setLocation] = useState<LocationObject>();
  const [weatherData, setWeatherData] = useState<WeatherData>();
  const [loadingData, setLoadingData] = useState(false);

  const getLocation = async () => {
    const { status } = await Location.requestBackgroundPermissionsAsync();
    if (status !== 'granted') {
      Alert.alert('A Solicatacao foi negada.');
      return;
    }
    const response = await Location.getCurrentPositionAsync();
    setLocation(response);
  };

  const loadForecast = async (lat: number, long: number) => {
    setLoadingData(true);
    try {
      const data = await getCurrentWeatherData(lat, long);
      setWeatherData(data);
    } catch (err) {
      Alert.alert('Erro ao buscar os dados.');
    } finally {
      setLoadingData(false);
    }
  };

  useEffect(() => {
    if ((location?.coords?.latitude, location?.coords?.longitude)) {
      loadForecast(location?.coords?.latitude, location?.coords?.longitude);
    }
  }, [location]);

  useEffect(() => {
    getLocation();
  }, []);

  if (!weatherData) {
    return (
      <ActivityContainer>
        <ActivityIndicator size="large" color="#ffffff" />
      </ActivityContainer>
    );
  }

  return (
    <Container>
      <Header>
        {!loadingData && weatherData && (
          <View style={{ flexDirection: 'row' }}>
            <HeaderLocation style={{ fontWeight: 'bold' }}>{weatherData.name},</HeaderLocation>
            <HeaderLocation> {weatherData.sys.country}</HeaderLocation>
          </View>
        )}
      </Header>

      <ImageBackground
        resizeMode="stretch"
        source={WorldMap}
        style={{
          width: '100%',
          height: '40%',
          flex: 1,
          alignItems: 'center',
        }}
      >
        {!loadingData ? (
          <Main>
            <Temperature>
              {weatherData?.main.temp && weatherData?.weather[0].description && (
                <>
                  <Text>{capitalize(weatherData?.weather[0].description)}</Text>
                  <TemperatureText>
                    {weatherData?.main.temp.toFixed(0)}
                    <TemperatureText style={{ color: '#F7d002' }}>º</TemperatureText>
                  </TemperatureText>
                </>
              )}
              <ReloadButton onPress={() => getLocation()}>
                <Text>Atualizar</Text>
              </ReloadButton>
            </Temperature>

            <Footer>
              {weatherData?.main && (
                <>
                  <Text>
                    Min: {weatherData?.main.temp_min.toFixed(0)}
                    <Text style={{ color: '#F7D002' }}>º</Text>
                  </Text>
                  <Text>
                    Max: {weatherData?.main.temp_max.toFixed(0)}
                    <Text style={{ color: '#F7D002' }}>º</Text>
                  </Text>
                </>
              )}
            </Footer>
          </Main>
        ) : (
          <ActivityContainer>
            <ActivityIndicator size="large" color="#ffffff" />
          </ActivityContainer>
        )}
      </ImageBackground>
    </Container>
  );
};
